const path = require('path')
const webpack = require('webpack')
const combineLoaders = require('webpack-combine-loaders')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

const NODE_ENV = 'development'

module.exports = {
    context: `${__dirname}/src`,
    entry: './app.jsx',
    output: {
        path: `${__dirname}/dist`,
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\.js[x]$/,
                exclude: /node_modules/,
                loader: 'babel'
            }, {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel'
            }, {
                test: /\.css$/,
                loader: 'style-loader'
            }, {
                test: /\.css/,
                loader: 'css-loader',
                query: {
                    modules: true,
                    camelCase: 'dashes',
                    localIdentName: '[name]__[local]___[hash:base64:5]'
                }
            }, {
                test: /\.css$/,
                loader: 'postcss'
            }
        ]
    },
    loaders: [{ loaders: ['react-hot'] }],
    postcss: () => {
        return [
            require('precss'),
            require('autoprefixer')
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            NODE_ENV: JSON.stringify(NODE_ENV)
        })
    ],
    node: {
        net: 'empty',
        tls: 'empty',
        dns: 'empty'
    },
    watch: true,
    devtool: 'cheap-inline-module-source-map'
}
