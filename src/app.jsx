import 'babel-polyfill'
import React from 'react'
import { render } from 'react-dom'
import { Router, Route, IndexRoute, hashHistory } from 'react-router'
import { Provider } from 'react-redux'
import { bindActionCreators } from 'redux'
import { syncHistoryWithStore } from 'react-router-redux'
import store from './store/store'
import Main from './main'
import { Dashboard } from './dashboard'
import { Owner } from './owner-page'
import { UserProfile } from './user-profile'
import { NotFound } from './notfound'
import { LoginPage, RegisterPage } from './entrance'
import { Auth } from './modules'
import { user as UserActions } from './store/actions'
import { AuthorisedPages } from './containers'

const storeInstance = store()
const history = syncHistoryWithStore(hashHistory, storeInstance)

const requireAuth = () => {
    if (Auth.isUserAuthenticated()) {
        console.log('token found', Auth.decodeToken())
        return Auth.decodeToken()
    }
    console.log('token lost')
    hashHistory.push('login')
    return null
}

const getUser = () => {
    const id = Auth.decodeToken().sub
    return storeInstance.dispatch(UserActions.fetch(id))
}

const routes = (
    <Provider store={storeInstance} >
        <Router history={history} >
            <Route path="/" component={Main} onEnter={requireAuth}>
                <IndexRoute component={Dashboard} />
                <Route path="dashboard" component={Dashboard} />
                <Route path="profile" component={UserProfile} onEnter={getUser} />
            </Route>
            <Route path="/login" component={LoginPage} />
            <Route path="/signup" component={RegisterPage} />
            <Route path="*" component={NotFound} />
        </Router>
    </Provider>
)

render(routes, document.getElementById('root'))
