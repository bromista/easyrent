import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import _ from 'lodash'
import * as actions from '../store/actions'


class RegisterPage extends Component {

    constructor(props) {
        super(props)
        this.onChangeLogin = this.onChangeLogin.bind(this)
        this.onChangePassword = this.onChangePassword.bind(this)
        this.onChangeFirstName = this.onChangeFirstName.bind(this)
        this.onChangeLastName = this.onChangeLastName.bind(this)
        this.onClickHandler = this.onClickHandler.bind(this)
        this.onChangeRole = this.onChangeRole.bind(this)
        this.state = {
            login: '',
            password: '',
            firstName: '',
            lastName: '',
            role: ''
        }
    }

    onChangeLogin(e) {
        this.setState({
            login: e.target.value
        })
    }

    onChangePassword(e) {
        this.setState({
            password: e.target.value
        })
    }

    onChangeFirstName(e) {
        this.setState({
            firstName: e.target.value
        })
    }

    onChangeLastName(e) {
        this.setState({
            lastName: e.target.value
        })
    }

    onClickHandler() {
        this.props.registerAction({
            login: this.state.login,
            password: this.state.password,
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            role: this.state.role
        })
    }

    onChangeRole(e) {
        this.setState({
            role: e.target.value
        })
    }

    render() {
        // console.log('PROPS', this.props)
        return (
            <div>
                {this.props.authError && <div>{this.props.authError.message}</div>}
                <p>
                    <span>Логин</span>
                    <input type="text" name="email" onChange={this.onChangeLogin} value={this.state.login} />
                </p>
                <p>
                    <span>Пароль</span>
                    <input type="text" name="password" onChange={this.onChangePassword} value={this.state.password} />
                </p>
                <p>
                    <span>Имя</span>
                    <input type="text" name="first_name" onChange={this.onChangeFirstName} value={this.state.firstName} />
                </p>
                <p>
                    <span>Фамилия</span>
                    <input type="text" name="last_name" onChange={this.onChangeLastName} value={this.state.lastName} />
                </p>
                <p>
                    <input id="owner" name="role" type="radio" value="owner" onChange={this.onChangeRole} />
                    <label htmlFor="owner" >хочу сдать квартиру</label>
                </p>
                <p>
                    <input id="tenant" name="role" type="radio" value="tenant" onChange={this.onChangeRole} />
                    <label htmlFor="tenant" >хочу снять квартиру</label>
                </p>
                <p>
                    <input type="submit" value="Зарегистрироваться" onClick={this.onClickHandler} />
                </p>
            </div>
        )
    }
}

function mapStateToProps(state) {
    console.log('STATE', state)
    return {
        authError: _.get(state, 'auth.error'),
        user: state.auth.payload
    }
}

function mapDispatchToProps(dispatch) {
    return {
        registerAction: bindActionCreators(actions.register, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterPage)
