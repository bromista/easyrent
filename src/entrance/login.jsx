import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as actions from '../store/actions'
import style from './style.css'


class LoginPage extends Component {

    constructor(props) {
        super(props)
        this.onChangeLogin = this.onChangeLogin.bind(this)
        this.onChangePassword = this.onChangePassword.bind(this)
        this.state = {
            login: '',
            password: ''
        }
    }

    onChangeLogin(e) {
        this.setState({
            login: e.target.value
        })
    }

    onChangePassword(e) {
        this.setState({
            password: e.target.value
        })
    }

    onClickHandler() {
        this.props.registerAction({ email: this.state.login, password: this.state.password })
    }

    render() {
        return (
            <div className={style.wrapper} >
                <div className={style.form} >
                    <div className={style.inpt} >
                        <input
                            placeholder="Email"
                            name="email"
                            type="text"
                            className="validate"
                            onChange={this.onChangeLogin}
                            value={this.state.login}
                        />
                        <label htmlFor="email" >Email</label>
                    </div>
                    <div className={style.inputField} >
                        <input
                            placeholder="Password"
                            name="password"
                            type="text"
                            className="validate"
                            onChange={this.onChangePassword}
                            value={this.state.password}
                        />
                        <label htmlFor="password" >Password</label>
                    </div>
                    <button
                        className="btn waves-effect waves-light" type="submit" name="action"
                        onClick={this.onClickHandler.bind(this)}
                    >
                        Войти
                    </button>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        type: state.auth.type,
        user: state.auth.payload
    }
}

function mapDispatchToProps(dispatch) {
    return {
        registerAction: bindActionCreators(actions.authorisation, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage)