import React, { Component } from 'react'
import { Link } from 'react-router'

export class NotFound extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div>
                <Link to="/" >Main</Link>
                <h1>NotFound</h1>
            </div>
        )
    }
}