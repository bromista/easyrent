import React, { Component, PropTypes } from 'react'
import { Link } from 'react-router'
import style from './style.css'

export default class Main extends Component {

    static propTypes = {
        children: PropTypes.object.isRequired
    }

    // static defaultProps = {
    //     children: {}
    // }

    render() {
        return (
            <div>
                <ul className={style.menu}>
                    <li><Link to="/" >Main</Link></li>
                    <li><Link to="/dashboard" >Dashboard</Link></li>
                    <li><Link to="/profile" >Profile</Link></li>
                    <li><Link to="/login" >Login</Link></li>
                    <li><Link to="/signup" >Register</Link></li>
                </ul>
                {this.props.children}
            </div>
        )
    }
}
