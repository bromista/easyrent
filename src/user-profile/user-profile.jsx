import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { user as userActions } from '../store/actions'
import OwnerProfile from './owner-profile.jsx'

const UserProfile = (props) => {
    // console.log('props', props)
    return (<OwnerProfile {...props} />)
}

UserProfile.propTypes = {}

UserProfile.defaultProps = {}

function mapStateToProps(state) {
    return {
        user: state.user
    }
}

function mapDispatchToProps(dispatch) {
    return {
        updateUser: bindActionCreators(userActions.update, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile)
