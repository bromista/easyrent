import React from 'react'
import { Field, reduxForm, FieldArray } from 'redux-form'
import { connect } from 'react-redux'

export class OwnerProfile extends React.Component {

    static propTypes = {}

    static defaultProps = {}

    renderApartments = ({ custom, fields, meta: { touched, error } }) => (
        <ul>
            <li>
                <button type="button" onClick={() => fields.push({})} >
                    +
                </button>
                {touched && error && <span>{error}</span>}
            </li>
            {fields.map((member, index) =>
                <li key={index} >
                    <button
                        type="button"
                        title="Remove Member"
                        onClick={() => fields.remove(index)}
                    >
                        -
                    </button>
                    <Field
                        name={`${member}.city`}
                        type="text"
                        component={this.renderField}
                        label="Город"
                    />
                    <Field
                        name={`${member}.street`}
                        type="text"
                        component={this.renderField}
                        label="Улица"
                    />
                    <Field
                        name={`${member}.house_number`}
                        type="text"
                        component={this.renderField}
                        label="Дом"
                    />
                    <Field
                        name={`${member}.apartment_number`}
                        type="text"
                        component={this.renderField}
                        label="Номер квартиры"
                    />
                </li>
            )}
        </ul>
    )

    renderField = ({ input, label, type, meta: { touched, error } }) => (
        <div>
            <div>
                <span>{label}</span>
                <input {...input} type={type} placeholder={label} />
                {touched && error && <span>{error}</span>}
            </div>
        </div>
    )

    renderMembers = ({ label, custom, fields, meta: { touched, error } }) => (
        <ul>
            <li>
                <button type="button" onClick={() => fields.push({})} >
                    +
                </button>
                {touched && error && <span>{error}</span>}
            </li>
            {fields.map((member, index) =>
                <li key={index} >
                    <button
                        type="button"
                        title="Remove Member"
                        onClick={() => fields.remove(index)}
                    >
                        -
                    </button>
                    <Field
                        name={`${member}.${custom}`}
                        type="text"
                        component={this.renderField}
                        label={label}
                    />
                </li>
            )}
        </ul>
    )

    onSubmit = (data) => {
        console.log('@@@', this.props)
        this.props.updateUser({ userId: this.props.user._id, user: data })
    }

    render() {
        const { handleSubmit, pristine, submitting } = this.props
        console.log('props', this.props)
        return (
            <form onSubmit={handleSubmit(this.onSubmit)} >
                <div>
                    <span>Имя</span>
                    <div>
                        <Field
                            name="first_name"
                            component="input"
                            type="text"
                            placeholder="Введите имя"
                        />
                    </div>
                </div>
                <div>
                    <span>Фамилия</span>
                    <div>
                        <Field
                            name="last_name"
                            component="input"
                            type="text"
                            placeholder="Введите фамилию"
                        />
                    </div>
                </div>
                <div>
                    <span>Отчество</span>
                    <div>
                        <Field
                            name="patronymic"
                            component="input"
                            type="text"
                            placeholder="Введите отчество"
                        />
                    </div>
                </div>
                <div>
                    <span>Email</span>
                    <div>
                        <FieldArray name="email" component={this.renderMembers} type="text" label="Email"
                                    custom="email" />
                    </div>
                </div>
                <div>
                    <span>Телефон</span>
                    <div>
                        <FieldArray name="phone" component={this.renderMembers} type="text" custom="phone" label="Phone" />
                    </div>
                </div>
                <div>
                    <span>Apartments</span>
                    <div>
                        <FieldArray name="apartments" component={this.renderApartments} />
                    </div>
                </div>
                <div>
                    <button
                        type="submit"
                        disabled={pristine || submitting || submitting}
                    >
                        Сохранить
                    </button>
                </div>
            </form>
        )
    }
}

OwnerProfile = reduxForm({
    form: 'userProfile',
    enableReinitialize: true
})(OwnerProfile)

OwnerProfile = connect(state => ({
        initialValues: state.user
    })
)(OwnerProfile)

export default OwnerProfile
