import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'

class Owner extends Component {
    render() {
        const { handleSubmit } = this.props
        return (
            <form onSubmit={handleSubmit}>
                <div>
                    <span>Имя</span>
                    <div>
                        <Field name="first_name" component="input" type="text" placeholder="Введите имя" />
                    </div>
                </div>
                <div>
                    <span>Фамилия</span>
                    <div>
                        <Field name="last_name" component="input" type="text" placeholder="Введите фамилию" />
                    </div>
                </div>
                <div>
                    <span>Отчество</span>
                    <div>
                        <Field name="patronymic" component="input" type="text" placeholder="Введите отчество" />
                    </div>
                </div>
            </form>
        )
    }
}

function mapStateToProps(state) {
    console.log('state', state)
    return {
        owner: state.user.payload
    }
}

export default connect(mapStateToProps)(Owner)
