import { createStore, applyMiddleware } from 'redux'
import rootReducer from './reducers'
import thunk from 'redux-thunk'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default function configureStore(initialState) {
    const store = createStore(
		rootReducer,
		initialState,
		composeEnhancers(
			applyMiddleware(thunk)
		)
	)
    if (module.hot) {
        module.hot.accept('./reducers', () => {
            const nextRootReducer = require('./reducers/index')
            store.replaceReducer(nextRootReducer)
        })
    }

	// if (module.hot) {
	//   // Enable Webpack hot module replacement for reducers
	//   module.hot.accept('../reducers', () => {
	//     const nextRootReducer = require('../reducers/index');
	//     store.replaceReducer(nextRootReducer);
	//   });
	// }

    return store
}
