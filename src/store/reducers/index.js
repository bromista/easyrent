import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import { reducer as formReducer } from 'redux-form';
import user from './user'
import auth from './auth'

export default combineReducers({
    user,
    auth,
    routing: routerReducer,
    form: formReducer
})
