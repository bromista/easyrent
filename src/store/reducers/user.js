import * as types from '../action-types.js'

export default (initialState = {}, action) => {
    switch (action.type) {
        case types.USER_INFO:
            return Object.assign({}, action.payload)
        case types.USER_PROFILE_SUBMIT:
            return action.payload
        default:
            return initialState
    }
}
