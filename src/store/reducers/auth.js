import * as types from '../action-types'

export default (initialState = {}, action) => {
    switch (action.type) {
        case types.AUTH:
            return Object.assign({}, action.payload)
        case types.REGISTER:
            return Object.assign({}, action.payload)
        default:
            return initialState
    }
}
