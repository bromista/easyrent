import axios from 'axios'
import { Auth } from '../../modules'
import * as types from '../action-types'

export function fetch(params) {
    console.log('user fetch PARAMS', params)
    // debugger
    const config = {
        method: 'post',
        url: 'http://localhost:3000/api/user',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `bearer ${Auth.getToken()}`
        },
        responseType: 'json',
        data: {
            userId: params
        }
    }
    return (dispatch) => {
        axios(config)
            .then((response) => {
                console.log('response', response)
                dispatch({
                    type: types.USER_INFO,
                    payload: response.data
                })
            })
            .catch((error) => {
                console.log('ERROR', error);
            })
    }
}

export function update(params) {
    console.log('Updating UP', params)
    const config = {
        method: 'post',
        url: 'http://localhost:3000/api/user/update',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `bearer ${Auth.getToken()}`
        },
        responseType: 'json',
        data: {
            userId: params.userId,
            user: params.user
        }
    }
    return (dispatch) => {
        axios(config)
            .then((response) => {
                console.log('response', response)
                dispatch({
                    type: types.USER_PROFILE_SUBMIT,
                    payload: response.data
                })
            })
    }
}
