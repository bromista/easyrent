import axios from 'axios'
import { hashHistory } from 'react-router'
import { Auth } from '../../modules'
import * as types from '../action-types'

export function authorisation(params) {
    const config = {
        method: 'post',
        url: 'http://localhost:3000/login',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `bearer ${Auth.getToken()}`
        },
        responseType: 'json',
        data: {
            login: params.email,
            password: params.password
        }
    }
    return (dispatch) => {
        axios(config)
            .then((response) => {
                console.log('response', response)
                Auth.authenticateUser(response.data.token)
                hashHistory.push('/dashboard')
                dispatch({
                    type: types.AUTH,
                    payload: response.data
                })
            })
            .catch((error) => {
                console.log('ERROR', error)
            })
    }
}

export function register(params) {
    const config = {
        method: 'post',
        url: 'http://localhost:3000/signup',
        headers: { 'Content-Type': 'application/json' },
        responseType: 'json',
        data: {
            login: params.login,
            password: params.password,
            first_name: params.firstName,
            last_name: params.lastName,
            role: params.role
        }
    }
    return (dispatch) => {
        axios(config)
            .then((response) => {
                // console.log('response', response)
                if (response.data.created && response.data.authorised) {
                    Auth.authenticateUser(response.data.token)
                    hashHistory.push('/dashboard')
                }
                dispatch({
                    type: types.REGISTER,
                    payload: response.data
                })
            })
            .catch((error) => {
                console.log('ERROR', error)
            })
    }
}
