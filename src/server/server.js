const express = require('express')
const app = express()
const http = require('http')
const mongoose = require('./lib/mongoose')
const bodyParser = require('body-parser')
const config = require('./config/index.js')
const passport = require('passport')
const api = require('./api')
const morgan = require('morgan')
const cookieParser = require('cookie-parser')
// const session = require('express-session')
const authCheckMiddleware = require('./middleware/auth-check')

/* DB CONNECTION */
const db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', () => {
    console.log('DB is connected')
})

/* APP CONFIGURING */
app.use('/api', authCheckMiddleware)
app.use(express.static('./dist'))
app.use(morgan('dev'))
app.use(cookieParser())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(passport.initialize())

require('./config').passportStrtegy(passport)
require('./api.js')(app, passport)

app.listen(3000, () => {
    console.log('Server run at localhost:3000')
})
