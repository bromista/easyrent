const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Owner = new Schema({
    id: String,
    login: String,
    password: String,
    first_name: String,
    last_name: String,
    patronymic: String,
    role: String,
    phone: [{ phone: String }],
    email: [{ email: String }],
    registration_date: String,
    last_login_date: String,
    apartments: [{
        city: String,
        street: String,
        house_number: String,
        apartment_number: String,
        info: String
    }]
})

module.exports = Owner
