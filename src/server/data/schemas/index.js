const Owner = require('./owner')
const User = require('./user')

module.exports.Owner = Owner
module.exports.User = User
