const mongoose = require('mongoose')
const bcrypt = require('bcrypt-nodejs')

const Schema = mongoose.Schema

const User = new Schema({
    login: String,
    password: String,
    first_name: String,
    last_name: String,
    patronymic: String,
    role: String,
    phone: [{ phone: String }],
    email: [{ email: String }],
    registration_date: String,
    last_login_date: String,
    apartments: [{
        city: String,
        street: String,
        house_number: String,
        apartment_number: String,
        info: String
    }]
})

User.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.password)
}

User.methods.generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null)
}

module.exports = mongoose.model('User', User)
