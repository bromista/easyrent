const nconf = require('nconf')
const path = require('path')
const passportStrtegy = require('./passport')

nconf.argv()
    .env()
    .file({ file: path.join(__dirname, 'config.json') })

module.exports.settings = nconf
module.exports.passportStrtegy = passportStrtegy
