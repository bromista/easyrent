const jwt = require('jsonwebtoken')
const LocalStrategy = require('passport-local').Strategy
const User = require('../data/schemas/user')
const config = require('./index.js')


module.exports = function (passport) {
    passport.serializeUser((user, done) => {
        done(null, user.id)
    })
    passport.deserializeUser((id, done) => {
        User.findById(id, (err, user) => {
            done(err, user)
        })
    })

    /* LOGIN STRATEGY */
    passport.use('local-login', new LocalStrategy({
            session: false,
            usernameField: 'login',
            passwordField: 'password',
            passReqToCallback: true
        },
        (req, login, password, done) => {
            console.log('MONGO1', login, password)
            User.findOne({ 'login': login }, (err, user) => {
                if (err) {
                    console.log('ERROR')
                    return done(err)
                }
                if (!user) {
                    console.log('ERROR. Incorrect username')
                    return done(null, false, { message: 'Incorrect username.' })
                }
                if (!user.validPassword(password)) {
                    console.log('ERROR. Incorrect password')
                    return done(null, false, { message: 'Incorrect password.' })
                }
                console.log('USER', user)

                const payload = {
                    sub: user._id
                }

                // create a token string
                const token = jwt.sign(payload, config.settings.get('jwtSecret'))
                return done(null, user, token)
            })
        })
    )
}
