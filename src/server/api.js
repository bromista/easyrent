const async = require('async')
const jwt = require('jsonwebtoken')
const moment = require('moment')
const config = require('./config/index.js')
const User = require('./data/schemas/user')

module.exports = function (app, passport) {
    app.post('/api/user', (req, res) => {
        console.log('/api/user', req.body)
        const userId = req.body.userId
        async.waterfall([
            function (callback) {
                User.findOne({ '_id': userId }, callback)
            },
            function (user) {
                if (!user) {
                    return res.send({
                        error: {
                            message: 'can not find such user'
                        }
                    })
                }
                return res.send(user)
            }
        ], (err, result) => {
            if (err) {
                console.log(err)
            } else {
                console.log(result)
            }
        })
    })

    app.post('/api/user/update', (req, res) => {
        console.log('/api/user/update', req.body)
        const { userId, user: { first_name, last_name, patronymic, email, phone, apartments } } = req.body
        User.findByIdAndUpdate(
            userId,
            {
                $set: {
                    first_name,
                    last_name,
                    patronymic,
                    email,
                    phone,
                    apartments
                }
            },
            { new: true },
            (err, user) => {
                if (err) {
                    return res.send({
                        error: {
                            message: 'can not find such user2'
                        }
                    })
                }
                return res.send(user)
            }
        )
    })

    app.post('/login', passport.authenticate('local-login'), (req, res) => {
        if (!req.user) {
            console.log('not user')
            return res.send({ status: 'wrong email' })
        }
        req.logIn(req.user, (err) => {
            if (err) {
                console.log('error during logIn')
                return next(err)
            }
            console.log('logged in')
            return res.send({
                authorised: true,
                token: req.authInfo
            })
        })
    })

    app.post('/signup', (req, res) => {
        console.log(req.body)
        const login = req.body.login
        const password = req.body.password
        const firstName = req.body.first_name
        const lastName = req.body.last_name
        const role = req.body.role

        async.waterfall([
            function (callback) {
                User.findOne({ 'login': login }, callback)
            },
            function (user) {
                if (user) {
                    console.log('we have email like that')
                    res.send({
                        error: {
                            message: 'user exists'
                        }
                    })
                } else {
                    const registrationDate = moment().format()
                    const newUser = new User()
                    newUser.login = login
                    newUser.password = newUser.generateHash(password)
                    newUser.first_name = firstName
                    newUser.last_name = lastName
                    newUser.role = role
                    newUser.email = { 'email': login }
                    newUser.registration_date = registrationDate
                    newUser.last_login_date = registrationDate
                    newUser.save((err, savedUser) => {
                        if (err) {
                            console.log('error during save')
                        } else {
                            console.log('success')
                            const payload = {
                                sub: savedUser._id
                            }
                            // create a token string
                            const token = jwt.sign(payload, config.settings.get('jwtSecret'))
                            console.log('REG_TOKEN', token)
                            return res.send({
                                created: true,
                                authorised: true,
                                token
                            })
                        }
                    })
                }
            }
        ], (err, result) => {
            if (err) {
                console.log(err)
            } else {
                console.log(result)
            }
        })
    })
}
