const mongoose = require('mongoose');
const config = require('../config');

mongoose.connect(config.settings.get('mongoose:uri'))

module.exports = mongoose
